library("data.table")
library("DESeq2")
library(dplyr)

setwd("")

exp_input <- ""

################################################################################
### RUN DESEQ2 ###
################################################################################
exp <- fread(exp_input)

colnames(exp)

#REORDER IF NECESSARY, FIRST SAMPLES CONDITION 1
#exp <- exp[,c(1,5,6,7,2,3,4)]
colnames(exp)

#For 1/2 lfc results
condition1 <- "Infected"
condition2 <- "Uninfected"

study <- "dcorgs1_inf_uninf"

################################################################################
### Run DESeq2 on new samples ###
################################################################################
#exp <- combined_exp

colnames(exp)[1] <- "genes"
exp <- as.data.frame(exp)
exp = exp[!duplicated(exp$genes),]
genes <- as.character(exp[[1]])
row.names(exp) <- genes
exp <- exp[,c(2:7)]


meta <- data.frame(matrix(ncol=1, nrow=6))
rownames(meta) <- colnames(exp) 
colnames(meta) <- c("condition")
meta[[1]] <- factor(c(rep(condition1, 3), rep(condition2, 3)))

#Create DESeq2 DDS object
ddsfull <- DESeqDataSetFromMatrix(
  countData = exp,
  colData = meta,
  design = ~ condition)

exp_colsum <- colSums(exp)
exp_cpm <- as.data.table(matrix(ncol=ncol(exp), nrow=nrow(exp)))
for (col in 1:length(exp_colsum)){exp_cpm[[col]] <- (exp[[col]]*1e6)/exp_colsum[col]}
keep <- rowSums(exp_cpm > 1) >= 2
ddsfull <- ddsfull[keep,]
exp <- exp[keep,]
genes <- row.names(exp)

#Normalize, run DESeq, contrast on infected v uninfected, treated v infected 
ddsfull <- DESeq(ddsfull, parallel=T) 
#For categorical comparison
res <- results(ddsfull,contrast=c("condition",condition1,condition2), alpha=0.05)
#for continuous results
resOrdered <- res[order(res$padj),]
resOrdered <- as.data.frame(resOrdered)
resOrdered <- cbind(rownames(resOrdered), resOrdered)
colnames(resOrdered)[1] <- "genes"

fwrite(resOrdered, file=paste0("output/deseq/", study, ".csv"),col.names = T)
